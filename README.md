# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - QR code component solution](#frontend-mentor---qr-code-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
    - [Useful resources](#useful-resources)
  - [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### Screenshot

![](./screenshots/screenshot-1.png)

### Links

- Solution URL: [https://www.frontendmentor.io/solutions/qr-code-component-using-bem-and-css-variables-ZYXW8womar](https://www.frontendmentor.io/solutions/qr-code-component-using-bem-and-css-variables-ZYXW8womar)
- Live Site URL: [https://cindrmons-cradle.gitlab.io/frontend-mentor/qr-code-component/](https://cindrmons-cradle.gitlab.io/frontend-mentor/qr-code-component/)

## My process

### Built with

- Semantic HTML5 markup
- CSS Variables / CSS Custom Properties
- Flexbox

### What I learned

What I've learned throughout this entire process that implementing design can be easy, but designing the actual stuff before implementing it can be a real time-consumer. I've also been currently practicing some BEM principles and CSS Variables, and I've learned that they can be really usefull at times and you don't really necessarily need to use preprocessors to get the job done. When you want something done quick, you can just use it with the minimal amount of tooling as much as possible, and you can truly practice and master a craft when you work from the smallest things and work your way up.


### Continued development

I would like to work more on CSS and just vanilla everything (vanilla CSS and JS), and possibly Elm for front-end web development in the coming future.

### Useful resources

- [The BEM Methodology](https://en.bem.info/methodology/css/) - The BEM Methodology foor reference.
- [Why I use the BEM naming convention for my CSS by Ken Powell](https://www.youtube.com/watch?v=SLjHSVwXYq4) - Where I got the inspiration for BEM methodology.
- [CSS Variables from W3 Schools](https://www.w3schools.com/css/css3_variables.asp) - Reference for CSS Variables.

## Author

- GitLab [@cindrmon](https://gitlab.com/cindrmon)
- Frontend Mentor - [@cindrmon](https://www.frontendmentor.io/profile/cindrmon)
- LinkedIn - [Gabriel Francia](https://www.linkedin.com/in/gdfrancia/)
- Website - Under Cnstruction
- Github - Don't really want to promote my Github. Rather wanna focus on GitLab more.
